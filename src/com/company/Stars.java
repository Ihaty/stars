package com.company;

public class Stars {
    private int count = 7;
    String[][] arrays = new String[7][7];

    public Stars() {
        taskOne();
        taskTwo();
        taskThree();
        taskFour();
        taskFive();
        taskSix();
        taskSeven();
        taskEight();
        taskNine();
        taskTen();
        taskEleven();
    }

    public void taskOne() {
        System.out.println("\nTask one:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                arrays[i][j] = "*";
                System.out.print(" " + arrays[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void taskTwo() {
        System.out.println("\nTask two:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 0 || i == 6 || j == 0 || j == 6) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }

    public void taskThree() {
        System.out.println("\nTask three:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 0 || j == 0 || j == 6 - i) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }

    public void taskFour() {
        System.out.println("\nTask four:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 6 || j == 0 || j == 0 + i) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }

    public void taskFive() {
        System.out.println("\nTask five:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 6 || j == 6 || i == 6 - j) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }

    public void taskSix() {
        System.out.println("\nTask six:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 0 || j == 6 || j == 0 + i) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }

    public void taskSeven() {
        System.out.println("\nTask seven:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 6 - j || i == 0 + j) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }

    public void taskEight() {
        System.out.println("\nTask eight:");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 0 || i == 0 + j || j == 6 - i) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }

    public void taskNine() {
        System.out.println("\nTask nine:");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 3 || j == 3 - i || j == 3 + i) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }

    public void taskTen() {
        System.out.println("\nTask ten:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                if (j == 3 || i == 3 + j || i == 3 - j) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }

    public void taskEleven() {
        System.out.println("\nTask eleven:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                if (j == 0 || i == 0 + j || i == 6 - j) {
                    arrays[i][j] = "*";
                    System.out.print(" " + arrays[i][j] + " ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();

        }
    }
}
